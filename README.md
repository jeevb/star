# staR

A human-friendly wrapper around [STAR](https://github.com/alexdobin/STAR "STAR") in R.

##### Installation:
```
## Install the required packages from Bioconductor
source('http://bioconductor.org/biocLite.R')
biocLite(c('GenomicAlignments', 'GenomicFeatures', 'Rsamtools'))

## Install Hadley Wickham's devtools package
install.packages('devtools')

## Install staR
devtools::install_bitbucket('jeevb/staR')
```

##### Example:
```r
library(staR)

## Specify settings for alignment
settings <- Settings(
    genomeFasta = '~/genomes/Homo_sapiens.GRCh38.81/Homo_sapiens.GRCh38.dna.primary_assembly.fa',
    annotation = '~/genomes/Homo_sapiens.GRCh38.81/Homo_sapiens.GRCh38.81.gtf',
    genomeOutDir = '~/Workspace/STARIndex',
    inputDir = 'reads',
    inputPattern = '.*_R(\\d)\\.fastq\\.gz$',
    pairedEnd = TRUE,
    readLength = 100,
    maxMultimapCount = 20,
    maxIntronSize = 1000000, # Set to 1 for small-RNAseq
    uncompressInput = 'zcat',
    output = 'output',
    outputPattern = '(.*)_Aligned\\.sortedByCoord\\.out\\.bam$',
    countMultimapped = FALSE,
    countsPath = 'counts.rda',
    threads = 8
    )

## Initialize project with specified settings
a <- STARproject(settings = settings)

## Generate the genome index if necessary
GenGenomeIndex(a)

## Perform alignment of reads to the genome
AlignReads(a)

## Generate and output counts matrix
a <- Enumerate(a)
```
